Die Sprache für Variablen, Methodenbenennung und Kommentare is Englisch

Alle Variablen- und Methodennamen sind CamelCase formatiert

Klassen- und Schnittstellennamen beginnen mit einem Großbuchstaben und Methoden- und Variablennamen mit einem Kleinbuchstaben. Namen von Konstanten werden nur mit Großbuchstaben geschrieben. Der Name der Klasse, die die Methode „main ()“ enthält, sollte auf „Main“ enden (z. B. „ProducerConsumerMain“). 

Jede Datei behinhaltet nur eine Klasse

Jede umfangreichere Methode beginnt mit einem Kopf, der mindestens eine Beschreibung der Methode sowie der Eingabeparameter, des Rückgab ewertes und der Fehlerausgänge enthält. Kleinere Methoden (weniger als 10 Zeilen Code) dürfen im Kopf der Klasse beschrieben werden.

Das Programm wird kommentiert. Es werden nur sinnvolle Kommentare verwendet, die nicht unmittelbar aus dem Programmtext erkennbar sind. Kommentare sollen das Programm leichter lesbar machen.
Beispiel: 
    falsch:
        val = val + 1;            /* inkrementiere den Wert von val*/
        System.out.printf ("\n"); /* Leerzeile ausgeben            */
    richtig: 
        String hostAddr;          /* Name oder IP Adresse          */
    
Jeder neue Programm-Block wird um 4 Zeichen eingerückt

Binäre und ternäre Operatoren und Operanden werden durch ein Leerzeichen getrennt 

Ausdrücke werden ggf. durch Klammern strukturiert (besser lesbar).
Die Anweisung 
    while ((table[i][0] != c) && (i < tab_len)) 
ist verständlicher als die 
hinreichende Java Anweisung 
    while (table[i][0] != c && i < tab_len) 
    
Je Zeile ist nur eine Anweisung erlaubt.
