CREATE TABLE `Rezepte` (
	`RID`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`Name`	TEXT NOT NULL,
	`Author`	TEXT NOT NULL,
	`Instruction`	INTEGER NOT NULL,
	`Favorite`	INTEGER,
	`Categorie`	INTEGER NOT NULL,
	`Bild`	TEXT
);

CREATE TABLE `Zutaten` (
	`ZID`	INTEGER NOT NULL,
	`Ingredients`	TEXT NOT NULL,
	FOREIGN KEY(`ZID`) REFERENCES `Rezepte`(`RID`) ON DELETE CASCADE,
	PRIMARY KEY(`ZID`)
);