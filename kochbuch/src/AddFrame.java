

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * Erstellt Joshua on 24.07.2017.
 * Bearbeitet von Diagne, Jeff, Joshua
 *
 * Klasse zum Hinzufuegen von Rezepten
 */
public class AddFrame extends JPanel {

    private DBConnect dbc;

    String[] tags = {"Vegetarisch",
            "Vegan",
            "Laktosefrei",
            "Glutenfrei",
            "Getränk",
            "Backwaren",
            "Normal"};
    JLabel lblName = new JLabel("Name");
    JTextField txtName = new JTextField();
    JLabel lblAuthor = new JLabel("Author");
    JTextField txtAuthor = new JTextField();
    JLabel lblPath = new JLabel("Picture");
    JTextField txtPath = new JTextField();
    JLabel lblVideo = new JLabel("Video Link");
    JTextField txtVideo = new JTextField();
    JLabel lblZutaten = new JLabel("Ingredients");
    JTextArea txtZutaten = new JTextArea();
    JLabel lblInstructions = new JLabel("Instructions");
    JTextArea txtInstructions = new JTextArea();
    JLabel lblFavorite = new JLabel("Favorite");
    JCheckBox chkboxFavorite = new JCheckBox();
    JButton btnSave = new JButton("Save");
    JLabel lblTags = new JLabel("Categorie");
    JComboBox TagList = new JComboBox(tags);
    JButton btnOpen = new JButton("Choose picture");


    public AddFrame(DBConnect dbc){
        this.dbc = dbc;

        String path;
        btnSave.setFont(new Font("calibri", Font.BOLD, 14));

        btnSave.setBackground(new Color(204,255,255));
        btnSave.setContentAreaFilled(false);
        btnSave.setOpaque(true);

        btnOpen.setBackground(new Color(204,255,255));
        btnOpen.setContentAreaFilled(false);
        btnOpen.setOpaque(true);

        TagList.setFont(new Font("calibri", Font.BOLD, 14));

    	this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(10,5,5,5);
    	this.setBackground(new Color(0,153, 153));


        gbc.gridx=0;
        gbc.gridy=0;
        this.add(lblName, gbc);
        lblName.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx=1;
        gbc.gridy=0;
        this.add(txtName, gbc);

        gbc.gridx=0;
        gbc.gridy=6;
        this.add(lblPath, gbc);
        lblPath.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx=0;
        gbc.gridy=7;
        this.add(lblVideo, gbc);
        lblVideo.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx=1;
        gbc.gridy=6;
        this.add(txtPath, gbc);

        gbc.gridx=1;
        gbc.gridy=7;
        this.add(txtVideo, gbc);

        gbc.gridx=0;
        gbc.gridy=1;
        this.add(lblAuthor, gbc);
        lblAuthor.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx=1;
        gbc.gridy=1;
        this.add(txtAuthor, gbc);


        gbc.gridx=0;
        gbc.gridy=2;
        this.add(lblTags, gbc);
        lblTags.setFont(new Font("calibri", Font.BOLD, 14));

        TagList.setSelectedIndex(6);
        gbc.gridx=1;
        gbc.gridy=2;
        this.add(TagList, gbc);

        gbc.gridx=0;
        gbc.gridy=3;
        this.add(lblFavorite, gbc);
        lblFavorite.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx=1;
        gbc.gridy=3;
        chkboxFavorite.setBackground(new Color(0,153, 153));
        this.add(chkboxFavorite, gbc);

        gbc.gridx=0;
        gbc.gridy=4;
        this.add(lblZutaten, gbc);
        lblZutaten.setFont(new Font("calibri", Font.BOLD, 14));



        txtZutaten.setRows(10);
        txtZutaten.setColumns(40);
        txtZutaten.setLineWrap(true);
        txtZutaten.setWrapStyleWord(false);
        JScrollPane zutatenPane = new JScrollPane(txtZutaten);
        zutatenPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        gbc.gridx=1;
        gbc.gridy=4;
        this.add(zutatenPane, gbc);

        gbc.gridx=0;
        gbc.gridy=5;
        this.add(lblInstructions, gbc);
        lblInstructions.setFont(new Font("calibri", Font.BOLD, 14));

        txtInstructions.setRows(20);
        txtInstructions.setColumns(70);
        txtInstructions.setLineWrap(true);
        txtInstructions.setWrapStyleWord(false);

        JScrollPane scrollPane = new JScrollPane(txtInstructions);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);


        gbc.gridx=1;
        gbc.gridy=5;
        this.add(scrollPane, gbc);


        // Save Button

        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtName.getText();
                String author = txtAuthor.getText();
                String ingredients = txtZutaten.getText();
                String instruction = txtInstructions.getText();
                String pictures = txtPath.getText();
                String video = txtVideo.getText();
                int favorite = 0;
                int category = TagList.getSelectedIndex();

                if (chkboxFavorite.isSelected()) {
                	favorite=1;
                } else {
                    favorite=0;
                }

                try {
                    if(txtName.getText().length() != 0 && txtZutaten.getText().length() != 0 && txtInstructions.getText().length() != 0) {
                        //DBConnect db = new DBConnect();
                        Recipe newRecipe = new Recipe(name, author, favorite, category, pictures, video, ingredients, instruction);
                        //if(db.insertRezepte(newRecipe.getName(), newRecipe.getAuthor(), newRecipe.getIngredients(),
                        //                 newRecipe.getInstruction(), newRecipe.getFavorite(),
                        //                 newRecipe.getCategory(), newRecipe.getPictures(), newRecipe.getVideo())){

                        //if(db.insertRezepte(newRecipe)) {
                        if(dbc.insertRezepte(newRecipe)) {

                            txtAuthor.setText(newRecipe.getAuthor());


                            txtName.setText("");
                            txtZutaten.setText("");
                            txtInstructions.setText("");
                            txtVideo.setText("");
                            txtPath.setText("");
                            chkboxFavorite.setSelected(false);

                            JOptionPane.showMessageDialog(null,
                                    "The Recipe was successfully saved.",
                                    "Information",
                                    JOptionPane.INFORMATION_MESSAGE);

                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "The Recipe's name exists already, Please choose another one",
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);

                        }


                    }else{
                        JOptionPane.showMessageDialog(null,
                                "You need to fill name, ingredients and instructions!",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } catch (SQLException e1) {
                    ErrorHandler er = new ErrorHandler();
                    er.writeLog("AddReciepe", e1.toString());;
                }

            }
        });

        btnOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Bilder",
                        "gif", "png", "jpg");
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(filter);
                int rueckgabeWert = chooser.showOpenDialog(null);
                if(rueckgabeWert == JFileChooser.APPROVE_OPTION)
                {
                    txtPath.setText(chooser.getSelectedFile().getPath());
            }}
        });
        gbc.gridx=3;
        gbc.gridy=6;
        this.add(btnOpen, gbc);


       try {
            String author;
            ResultSet rs;
            //DBConnect db = new DBConnect();
            //rs = db.getAuthor();
            rs = dbc.getAuthor();
            author = rs.getString(1);
            txtAuthor.setText(author);
            rs.close();

        } catch (SQLException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("AddReciepe", e.toString());
        }

        gbc.gridwidth= 2;
		gbc.gridx=0;
		gbc.gridy=8;
        this.add(btnSave, gbc);
       // btnSave.setBorder(BorderFactory.createLineBorder(new Color(24,116,205), 3));
    }

}
