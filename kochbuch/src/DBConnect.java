

import java.sql.*;

import javax.swing.*;

/**
 * @author Diagne, Meris, Joshua
 *
 */
public class DBConnect {

    private static final int VEGETARISCH = 0;
    private static final int VEGAN = 1;
    private static final int LAKTOSEFREI = 2;
    private static final int GLUTENFREI = 3;
    private static final int GETRAENK = 4;
    private static final int BACKWAREN = 5;
    private static final int NORMAL = 6;
    private static String query = "Select * " +
                                  "from Rezepte";
    private static Connection connection;

    /**
     * Constructor to create a new Connection.
     * @throws SQLException
     * */
    public DBConnect() throws SQLException {

        this.connection = DriverManager.getConnection("jdbc:sqlite:"+System.getProperty("user.dir")+"//Datenbank//Kochbuch.db");
    }

    /**
     * The method is used to get all recipes.
     * @throws SQLException
     */
    public ResultSet selectAllRezepte() throws SQLException {
        String query1 =  "select * " +
                         "from Rezepte r, Zutaten z " +
                         "where r.RID = z.ZID";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query1);
        return rs;
    }

    public void removeRezepte(int rid) throws SQLException {
        Recipe recipeToRemove = new Recipe(rid);
        String query1 =  "DELETE from Rezepte Where RID = '"+recipeToRemove.getRid()+"'";
        PreparedStatement stmt = connection.prepareStatement(query1);
        stmt.execute();
    }

    /**
     * This method display a specific recipe based on the following parameter.
     * @param name
     * @param author
     * @param categorie
     * @param favorite
     * @throws SQLException
     */
    public ResultSet selectSpecificRezepte(String name, String author, int favorite, int categorie) throws SQLException {
        Recipe recipeToFind = new Recipe(name, author, favorite, categorie);
        query =  " select  * " +
                 " from Rezepte r, Zutaten z " +
                 " where r.RID = z.ZID ";

        if (recipeToFind.getName() != "") {
            query += " AND (Name LIKE '%"+ recipeToFind.getName() +"%' OR Instruction LIKE '%" + recipeToFind.getName() + "%')";
        }

        if (recipeToFind.getFavorite() != 2) {
            query += " AND Favorite = '" + recipeToFind.getFavorite() +"'";
        }

        if (recipeToFind.getAuthor() != "") {
            query += " AND Author LIKE '%" + recipeToFind.getAuthor() +"%'";
        }


        if (recipeToFind.getCategory() == VEGETARISCH) {
            query += " AND Categorie = '" + VEGETARISCH + "'";

        } else if (recipeToFind.getCategory() == VEGAN) {
            query += " AND Categorie = '" + VEGAN + "'";

        } else if (recipeToFind.getCategory() == LAKTOSEFREI) {
            query += " AND Categorie = '" + LAKTOSEFREI + "'";

        } else if (recipeToFind.getCategory() == GLUTENFREI) {
            query += " AND Categorie = '" + GLUTENFREI + "'";

        } else if (recipeToFind.getCategory() == GETRAENK) {
            query += " AND Categorie = '" + GETRAENK + "'";

        } else if (recipeToFind.getCategory() == BACKWAREN) {
            query += " AND Categorie = '" + BACKWAREN + "'";
        } else if (recipeToFind.getCategory() == NORMAL) {
            query += " AND Categorie = '" + NORMAL + "'";
        }
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        return rs;
    }

  /*  public ResultSet checkName(String name) throws SQLException {
        String query1 = " SELECT Name " +
                        " FROM Rezepte " +
                        " WHERE Name = '"+name+"' ";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query1);

        return rs;
    }*/

    /**
     * This method is used to insert recipes in the Database.
     * @param name name of the recipe
     * @param instruction describtion of the recipe.
     * @param author The author of the recipe
     * @param category Categorie of recipe
     * @throws SQLException
     */
    //public Boolean insertRezepte(String name, String author, String ingredients, String instruction, int favorite, int category, String pictures, String video) throws SQLException{
    public Boolean insertRezepte(Recipe newRecipe) throws SQLException {
        //Recipe newRecipe = new Recipe(name, author, favorite, category, pictures, video, ingredients, instruction);

        String query1 = " SELECT Name " +
                        " FROM Rezepte " +
                        " WHERE Name = '"+newRecipe.getName()+"' ";
        Statement stmt2 = connection.createStatement();
        ResultSet rs = stmt2.executeQuery(query1);
        if(rs.next()){
            stmt2.close();
            return false;
        }else{
            String insertRezepte = "insert into Rezepte(Name, Author, Instruction, favorite, Categorie, Bild, Video)\n" +
                    "values('"+newRecipe.getName()+"', '"+newRecipe.getAuthor()+"', '"+newRecipe.getInstruction()+"', '"
                    +newRecipe.getFavorite()+"', '"+newRecipe.getCategory()+"', '"+newRecipe.getPictures()+"', '"+newRecipe.getVideo()+"')";
            PreparedStatement stmt = connection.prepareStatement(insertRezepte);
            stmt.execute();

            String insertZutaten = "insert into Zutaten(ZID, Ingredients) values((SELECT RID FROM Rezepte ORDER BY RID DESC LIMIT 1), '"+newRecipe.getIngredients()+"')";
            PreparedStatement stmt1 = connection.prepareStatement(insertZutaten);
            stmt1.execute();
            stmt1.close();
            stmt2.close();
            return true;
        }
    }

    public  ResultSet selectFavorite() throws SQLException {
        String query = " Select * " +
                       " From Rezepte r, Zutaten z " +
                       " Where r.RID = z.ZID AND Favorite = 1 ";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }

    public void setFavorite(int id, int state) throws SQLException{
        String query = "Update Rezepte " +
                       "Set Favorite = " + state + " " +
                       "Where RID = " + id;
        PreparedStatement stmt1 = connection.prepareStatement(query);
        stmt1.execute();
    }

    public ResultSet getFavorite(int id) throws SQLException{
        String query = " Select Favorite " +
                " From Rezepte" +
                " Where RID = " + id;
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }

    public ResultSet getAuthor() throws SQLException{
        String query = " SELECT Author " +
                       " FROM Rezepte " +
                       " ORDER BY RID DESC " +
                       " LIMIT 1 ";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        return rs;
    }

   /* public void setAuthor(String author) throws SQLException{
        String query = "Update Author " +
                "Set author = '" + author + "' " +
                "Where AID = " + 0;
        PreparedStatement stmt1 = connection.prepareStatement(query);
        stmt1.execute();
    }*/

    public void editRezept(int id, String name, String author, String instruction, String ingredients,
                           int favorite, int categorie, String path, String videoURL)throws SQLException{
        String query = " Update Rezepte " +
                " Set name = '" + name + "', " +
                " author = '" + author + "', " +
                " Instruction = '" + instruction + "', " +
                " Favorite = " + favorite + ", " +
                " Categorie = " + categorie + ", " +
                " Bild = '" + path + "', " +
                " Video = '" + videoURL + "' " +
                " Where RID = " + id;
        PreparedStatement stmt1 = connection.prepareStatement(query);
        stmt1.execute();

        String query1 = "Update Zutaten " +
                " Set Ingredients = '" + ingredients + "' " +
                "Where ZID = " + id;
        PreparedStatement stmt2 = connection.prepareStatement(query1);
        stmt2.execute();
    }

    public static void main(String[] args) throws SQLException {
          //DBConnect db = new DBConnect();
          //System.out.println(dbc.insertRezepte("wegf24gf","","","",0,1,"", ""));
    }

}
