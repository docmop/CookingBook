import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class EditFrame {

    private DBConnect dbc;

    String[] tags = {"Vegetarian",
            "Vegan",
            "Lactose free",
            "Gluten free",
            "Drink",
            "Bakery products",
            "Normal"};
    JLabel lblName = new JLabel("Name");
    JTextField txtName = new JTextField();
    JLabel lblAuthor = new JLabel("Author");
    JTextField txtAuthor = new JTextField();
    JLabel lblPath = new JLabel("Picture");
    JLabel lblVideo = new JLabel("Video Link");
    JTextField txtVideo = new JTextField();
    JTextField txtPath = new JTextField();
    JLabel lblZutaten = new JLabel("Ingredients");
    JTextArea txtZutaten = new JTextArea();
    JLabel lblInstructions = new JLabel("Instructions");
    JTextArea txtInstructions = new JTextArea();
    JLabel lblFavorite = new JLabel("Favorite");
    JCheckBox chkboxFavorite = new JCheckBox();
    JButton btnSave = new JButton("Save");
    JLabel lblTags = new JLabel("Categorie");
    JComboBox tagList = new JComboBox(tags);
    JButton btnOpen = new JButton("Change picture");

    public EditFrame(DBConnect dbc){
        this.dbc = dbc;
    }

    /**
     * Constructor
     * @param id id of Recipe
     * @param name name of Recipe
     * @param author author of Recipe
     * @param ingredients ingredients of Recipe
     * @param instructions instructions of Recipe
     * @param favorite favorite of Recipe
     * @param categorie categorie of Recipe
     * @param path path of Recipe
     * @param videoURL videoURL of Recipe
     */
    public void buildEditFrame(int id, String name, String author, String ingredients, String instructions,
                               boolean favorite, int categorie, String path, String videoURL) {


        JFrame editFrame = new JFrame();
        editFrame.setLayout(new GridBagLayout());
        JPanel editPanel = new JPanel();
        ImageIcon img = new ImageIcon(System.getProperty("user.dir")+"/Icons/cook.png");
        editFrame.setIconImage(img.getImage());

        btnSave.setFont(new Font("calibri", Font.BOLD, 14));
        btnSave.setBackground(new Color(204, 255, 255));
        btnSave.setContentAreaFilled(false);
        btnSave.setOpaque(true);

        btnOpen.setBackground(new Color(204,255,255));
        btnOpen.setContentAreaFilled(false);
        btnOpen.setOpaque(true);

        tagList.setFont(new Font("calibri", Font.BOLD, 14));

        editPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(10, 5, 5, 5);
        editPanel.setBackground(new Color(0, 153, 153));

        gbc.gridx = 0;
        gbc.gridy = 0;
        editPanel.add(lblName, gbc);
        lblName.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx = 1;
        gbc.gridy = 0;

        editPanel.add(txtName, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        editPanel.add(lblAuthor, gbc);
        lblAuthor.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx = 1;
        gbc.gridy = 1;
        editPanel.add(txtAuthor, gbc);


        gbc.gridx = 0;
        gbc.gridy = 2;
        editPanel.add(lblTags, gbc);
        lblTags.setFont(new Font("calibri", Font.BOLD, 14));

        tagList.setSelectedIndex(6);
        gbc.gridx = 1;
        gbc.gridy = 2;
        editPanel.add(tagList, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        editPanel.add(lblFavorite, gbc);
        lblFavorite.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx = 1;
        gbc.gridy = 3;
        chkboxFavorite.setBackground(new Color(0, 153, 153));
        editPanel.add(chkboxFavorite, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        editPanel.add(lblZutaten, gbc);
        lblZutaten.setFont(new Font("calibri", Font.BOLD, 14));

        txtZutaten.setRows(10);
        txtZutaten.setColumns(40);
        txtZutaten.setLineWrap(true);
        txtZutaten.setWrapStyleWord(false);
        JScrollPane zutatenPane = new JScrollPane(txtZutaten);
        zutatenPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        gbc.gridx = 1;
        gbc.gridy = 4;
        editPanel.add(zutatenPane, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        editPanel.add(lblInstructions, gbc);
        lblInstructions.setFont(new Font("calibri", Font.BOLD, 14));



        JScrollPane scrollPane = new JScrollPane(txtInstructions);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);


        gbc.gridx = 1;
        gbc.gridy = 5;
        editPanel.add(scrollPane, gbc);

        gbc.gridx = 0;
        gbc.gridy = 6;
        editPanel.add(lblPath, gbc);
        lblPath.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx = 1;
        gbc.gridy = 6;
        editPanel.add(txtPath, gbc);

        gbc.gridx = 3;
        gbc.gridy = 6;
        editPanel.add(btnOpen, gbc);

        gbc.gridx = 0;
        gbc.gridy = 7;
        editPanel.add(lblVideo, gbc);
        lblVideo.setFont(new Font("calibri", Font.BOLD, 14));

        gbc.gridx = 1;
        gbc.gridy = 7;
        editPanel.add(txtVideo, gbc);



        gbc.gridwidth =  2;
        gbc.gridx = 0;
        gbc.gridy = 8;
        editPanel.add(btnSave, gbc);



        btnOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Bilder",
                        "gif", "png", "jpg");
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(filter);
                int rueckgabeWert = chooser.showOpenDialog(null);

                if(rueckgabeWert == JFileChooser.APPROVE_OPTION)
                {
                    txtPath.setText(chooser.getSelectedFile().getPath());
                }}
        });

        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtName.getText();
                String author = txtAuthor.getText();
                String zutaten = txtZutaten.getText();
                String instructions = txtInstructions.getText();
                String path = txtPath.getText();
                String videoURL = txtVideo.getText();
                int favorite = 0;
                int categorie = tagList.getSelectedIndex();

                if (chkboxFavorite.isSelected()) {
                    favorite = 1;
                } else {
                    favorite = 0;
                }

                try {
                    if(txtName.getText().length() != 0 && txtZutaten.getText().length() != 0 && txtInstructions.getText().length() != 0) {
                        //DBConnect db = new DBConnect();
                        //db.editRezept(id, name, author, instructions, zutaten, favorite, categorie, path, videoURL);
                        dbc.editRezept(id, name, author, instructions, zutaten, favorite, categorie, path, videoURL);


                        JOptionPane.showMessageDialog(null, "Recipe was successfully updated!");
                        txtName.setText("");
                        txtZutaten.setText("");
                        txtInstructions.setText("");
                        txtPath.setText("");
                        txtVideo.setText("");
                        chkboxFavorite.setSelected(false);
                    }else{
                        JOptionPane.showMessageDialog(null, "You need to fill name, ingredients and instructions!");

                    }
                } catch (SQLException e1) {
                    ErrorHandler er = new ErrorHandler();
                    er.writeLog("EditFrame", e1.toString());
                }

            }
        });
        
        editPanel.add(btnSave, gbc);

        txtName.setText(name);
        txtAuthor.setText(author);
        txtZutaten.setText(ingredients);
        txtInstructions.setText(instructions);
        chkboxFavorite.setSelected(favorite);
        tagList.setSelectedIndex(categorie);
        txtPath.setText(path);
        txtVideo.setText(videoURL);

        txtInstructions.setRows(15);
        txtInstructions.setColumns(70);
        txtInstructions.setLineWrap(true);
        txtInstructions.setWrapStyleWord(false);

        editFrame.setDefaultCloseOperation( WindowConstants.HIDE_ON_CLOSE);
        editFrame.add(editPanel);
        editFrame.setSize(1500,1050);
        editFrame.setVisible(true);
    }
}


