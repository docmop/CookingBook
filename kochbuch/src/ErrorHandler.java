import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by kodiak(Joshua Wiesen)
 *
 * Class to write terminaloutput to files
 */
public class ErrorHandler {

    public ErrorHandler() {
    }

    /**
     * write line to log
     * @param line line to write
     */
    public void writeLog(String ErrorFileName, String line) {
        FileWriter writer;
        File file;
        String timestamp;
        String filename;
        //timestamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS").format(date);
        filename = System.getProperty("user.dir") + "\\log\\" + "Error" + "_" + ErrorFileName + "_log.txt";
        file = new File(filename);
        try {
            writer = new FileWriter(file, true);
            writer.write(line);
            writer.write(System.getProperty("line.separator"));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}