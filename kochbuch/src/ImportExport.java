import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.*;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * Import and Export-Class
 * @author Phillip, Diagne
 */
public class ImportExport implements ImportExportInterface {

    private DBConnect dbc;

    public ImportExport(DBConnect dbc) {
        this.dbc = dbc;
    }

    /**
     * Method to export a recipe
     * @param recipe
     * @param filePath
     */
    public void exportRecipe(Recipe recipe, String filePath) {
        try {

            Element recipeElement = new Element("Recipe");
            Document doc = new Document(recipeElement);
            //doc.setRootElement(recipeElement);

            Element nameElement = new Element("Name");
            nameElement.setText(recipe.getName());
            doc.getRootElement().addContent(nameElement);

            Element authorElement = new Element("Author");
            authorElement.setText(recipe.getAuthor());
            doc.getRootElement().addContent(authorElement);

            Element ratingElement = new Element("Favorite");
            ratingElement.setText(Integer.toString(recipe.getFavorite()));
            doc.getRootElement().addContent(ratingElement);

            Element categoryElement = new Element("Category");
            categoryElement.setText(Integer.toString(recipe.getCategory()));
            doc.getRootElement().addContent(categoryElement);

            Element pictureElement = new Element("Picture");
            pictureElement.setText(recipe.getPictures());
            doc.getRootElement().addContent(pictureElement);

            Element videoElement = new Element("Video");
            videoElement.setText(recipe.getVideo());
            doc.getRootElement().addContent(videoElement);

            Element ingredientsElement = new Element("Ingredients");
            ingredientsElement.setText(recipe.getIngredients());
            doc.getRootElement().addContent(ingredientsElement);

            Element instructionsElement = new Element("Instruction");
            instructionsElement.setText(recipe.getInstruction());
            doc.getRootElement().addContent(instructionsElement);

            String xmlFile = filePath+".xml";
            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
           // xmlOutput.output(doc, System.out);
            xmlOutput.output(doc, new FileWriter(xmlFile));


        } catch (IOException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("ImportExport", e.toString());
        }

    }

    /**
     * Method to import a recipe
     * @param filePath
     */
    public Recipe importRecipe(String filePath) {
        Recipe recipeToImport = new Recipe();

        try {

            File inputFile = new File(filePath);
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(inputFile);
            Element classElement = document.getRootElement();

            recipeToImport.setName(			classElement.getChildText("Name"));
            recipeToImport.setAuthor(		classElement.getChildText("Author"));
            recipeToImport.setFavorite(		Integer.parseInt(classElement.getChildText("Favorite")));
            recipeToImport.setCategory(		Integer.parseInt(classElement.getChildText("Category")));
            recipeToImport.setPictures(		classElement.getChildText("Picture"));
            recipeToImport.setVideo(		classElement.getChildText("Video"));
            recipeToImport.setIngredients(	classElement.getChildText("Ingredients"));
            recipeToImport.setInstruction(	classElement.getChildText("Instruction"));
            
            //Recipe recipeToImport = new Recipe(name, author, favorite, category, picture, video, ingredients, instruction);

            try {
                //DBConnect db_old = new DBConnect();
                //if(db.insertRezepte(recipeToImport.getName(), recipeToImport.getAuthor(), recipeToImport.getIngredients(),
                //        recipeToImport.getInstruction(), recipeToImport.getFavorite(),
                //        recipeToImport.getCategory(), recipeToImport.getPictures(), recipeToImport.getVideo())){
                
                if(dbc.insertRezepte(recipeToImport)) {    

                    JOptionPane.showMessageDialog(null,
                            "The Recipe was successfully imported.",
                            "Information",
                            JOptionPane.INFORMATION_MESSAGE);

                } else {
                    JOptionPane.showMessageDialog(null,
                            "The Recipe's name exists already, Please choose another one",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);

                }
            } catch (SQLException e) {
                ErrorHandler er = new ErrorHandler();
                er.writeLog("ImportExport", e.toString());
            }


        } catch(JDOMException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("ImportExport", e.toString());
        } catch(IOException ioe) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("ImportExport", ioe.toString());
        }

        return recipeToImport;
    }

}