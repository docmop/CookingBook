/*
 * @Author Philip Hoffmann
 */

public interface ImportExportInterface {
	
	public void exportRecipe(Recipe recipe, String filePath);
	
	public Recipe importRecipe(String filePath);
}
