
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;

/**
 * @author Jeff, Joshua, Diagne
 */
public class Main {

    private static DBConnect dbc;
	
    public Main() throws IOException {

        try {
            dbc = new DBConnect();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to builde the frame
     */
    private void buildFrame(){
        //Main Frame

        JFrame frame = new JFrame( "Digitales Kochbuch" );
        ImageIcon img = new ImageIcon(System.getProperty("user.dir")+"/Icons/cook.png");
        frame.setIconImage(img.getImage());
        frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        JPanel contPanel = new JPanel();
        CardLayout cl = new CardLayout();
        SearchPanel SearchPanel = new SearchPanel(dbc);
        AddFrame AddPanel = new AddFrame(dbc);
        JScrollPane AddPane = new JScrollPane(AddPanel);
        contPanel.setLayout(cl);
        contPanel.add(SearchPanel, "2");
        contPanel.add(AddPane, "3");
        cl.show(contPanel,"2");

        //Menu Bar
        JMenuBar menuBar = new JMenuBar();
        //Button Save


        //Button Import

        JButton btnImport = new JButton("Import");
        ImageIcon imp = new ImageIcon(System.getProperty("user.dir")+"/Icons/import.png");
        btnImport.setIcon(imp);
        btnImport.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnImport.setHorizontalTextPosition(SwingConstants.CENTER);
        btnImport.setFont(new Font("calibri", Font.BOLD, 14));

        btnImport.setBackground(new Color(255,255,255));
        btnImport.setContentAreaFilled(false);
        btnImport.setOpaque(true);

        btnImport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter("XML Document (*.xml)", "xml");
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(filter);
                chooser.addChoosableFileFilter(filter);
                chooser.setAcceptAllFileFilterUsed(false);
                chooser.setDialogTitle("Import");
                chooser.setApproveButtonText("Import");
                chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                ImportExport ie = new ImportExport(dbc);
                int actionDialog = chooser.showOpenDialog(frame);
                if(actionDialog == JFileChooser.APPROVE_OPTION){
                    Recipe recipe = ie.importRecipe(chooser.getSelectedFile().getPath());
                    //db.insertRezepte(recipe);
                }
            }
        });
        menuBar.add(btnImport);



        //Button Add

        JButton btnAdd = new JButton("Add");
        ImageIcon add = new ImageIcon(System.getProperty("user.dir")+"/Icons/add.png");
        btnAdd.setIcon(add);
        btnAdd.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnAdd.setHorizontalTextPosition(SwingConstants.CENTER);
        btnAdd.setFont(new Font("calibri", Font.BOLD, 14));

        btnAdd.setBackground(new Color(255,255,255));
        btnAdd.setContentAreaFilled(false);
        btnAdd.setOpaque(true);

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(contPanel,"3");
            }
        });
        menuBar.add(btnAdd);

        //Button Search

        JButton btnSearch = new JButton("Search");
        ImageIcon sear = new ImageIcon(System.getProperty("user.dir")+"/Icons/search.png");
        btnSearch.setIcon(sear);
        btnSearch.setVerticalTextPosition(SwingConstants.BOTTOM);
        btnSearch.setHorizontalTextPosition(SwingConstants.CENTER);
        btnSearch.setFont(new Font("calibri", Font.BOLD, 14));

        btnSearch.setBackground(new Color(255,255,255));
        btnSearch.setContentAreaFilled(false);
        btnSearch.setOpaque(true);

        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cl.show(contPanel,"2");
            }
        });
        menuBar.add(btnSearch);

        //Add Items to Frame
        frame.add(contPanel);
        frame.setJMenuBar(menuBar);
        frame.setSize(1500,1050);
        frame.setVisible(true);
    }

    /**
     * Main Method
     * @param args
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.put("OptionPane.background",new ColorUIResource(0,153, 153));
            UIManager.put("Panel.background",new ColorUIResource(0,153, 153));
            UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("calibri",Font.BOLD,15)));
            UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("calibri",Font.BOLD,14)));

        } catch (ClassNotFoundException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("Main", e.toString());
        } catch (InstantiationException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("Main", e.toString());
        } catch (IllegalAccessException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("Main", e.toString());
        } catch (UnsupportedLookAndFeelException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("Main", e.toString());
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    Main m = new Main();
                    m.buildFrame();
                } catch (IOException e) {
                    ErrorHandler er = new ErrorHandler();
                    er.writeLog("Main", e.toString());
                }
            }
        });
    }
}
