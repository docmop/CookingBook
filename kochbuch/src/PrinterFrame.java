
import static javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.*;


/**
 * @author Joshua
 * edited by cheick 20.09.2017
 */
public class PrinterFrame {

    private DBConnect dbc;


    public PrinterFrame(DBConnect dbc){
        this.dbc = dbc;
    }


    /**
     * builds Frame
     * @param id id of recipe
     * @param name name of recipe
     * @param instruction instruction of recipe
     * @param ingredients ingredients of recipe
     * @param categorie categorie of recipe
     * @param path path of recipe
     * @param videoURL videoURL of recipe
     */
    public void buildFrame(int id, String name, String instruction, String ingredients, String categorie, String path, String videoURL){

        JFrame frame = new JFrame(name);
        frame.setDefaultCloseOperation( WindowConstants.HIDE_ON_CLOSE );
        frame.setLayout(new GridLayout(1,1));
        frame.setLayout(new BorderLayout());
        JPanel center = new JPanel();
        JPanel south = new JPanel();
        JPanel east = new JPanel();
        JEditorPane editorPane = new JEditorPane();
        frame.setVisible(true);
        editorPane.setEditable(false);
        editorPane.setContentType("text/html");

        editorPane.setText("<h1 style=\"font-family:calibri;\"><font color=\"0000ff\" size=\"10\">"+name+"</font></h1>" +
                           "<h1><font><font color=\"blue\">Category:</font></h1><p style=\"font-family:calibri;\"><font size=\"4\">"+categorie+"<font></p>"+
                           "<h1><font><font color=\"blue\">Ingredients:</font></h1>"+
                           "<font><font color=\"blue\"><pre style=font-family:calibri;><font size=\"4\">"+ingredients+"</font></pre>" +
                           "<h1><font ><font color=\"blue\">Instructions:</font></h1>" +
                           "<font><p style=\"font-family:calibri;\"><font size=\"4\">"+instruction+"</font></p>");
System.out.println(editorPane.getText());
       /* editorPane.setText("<h1 style=\"font-family:calibri;\"><font color=\"0000ff\" size=\"10\">"+name+"</font></h1>" +
                "<h1><font><font color=\"blue\">Category:</font></h1><p style=\"font-family:calibri;\"><font size=\"4\">"+categorie+"<font></p>"+
                "<h1><font><font color=\"blue\">Ingredients:</font></h1>"+
                "<font><font color=\"blue\"><pre style=font-family:calibri;><font size=\"4\">"+ingredients+"</font></pre>" +
                "<h1><font ><font color=\"blue\">Instructions:</font></h1>" +
                "<font><p style=\"font-family:calibri;\"><font size=\"4\">"+instruction+"</font></p>");
*/
        editorPane.setBackground(new Color(204,255,255));


        JScrollPane textPane = new JScrollPane(editorPane);
        textPane.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
        center.add(textPane);
        ImageIcon icon = new ImageIcon(path);
        JLabel label = new JLabel(icon);

        //Mathematical Stuff
        float height = icon.getIconHeight();
        float width = icon.getIconWidth();

        if(width<height) {
            float factor = 500 / height;
            width = java.lang.Math.round(width * factor);
            int widthInt = (int) width;
            icon.setImage(icon.getImage().getScaledInstance(widthInt, 500, Image.SCALE_DEFAULT));
        }else{
            float factor = 500 / width;
            height = java.lang.Math.round(height * factor);
            int heightInt = (int) height;
            icon.setImage(icon.getImage().getScaledInstance(500, heightInt, Image.SCALE_DEFAULT));
        }


        center.setLayout(new GridLayout(1,1));
        JButton btnPrint = new JButton("Print");
        btnPrint.setFont(new Font("calibri", Font.BOLD, 14));
        btnPrint.setBackground(new Color(204,255,255));
        btnPrint.setContentAreaFilled(false);
        btnPrint.setOpaque(true);

        btnPrint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PreviewDialog p = new PreviewDialog(editorPane);
                p.setVisible(true);
               /* Printer pp = new Printer();
                pp.addString(editorPane.getText());
                pp.druckeSeite(null, "sdds", true, false);*/
            }
        });



        JButton btnVideo = new JButton("Watch Video");
        btnVideo.setFont(new Font("calibri", Font.BOLD, 14));
        btnVideo.setBackground(new Color(204,255,255));
        btnVideo.setContentAreaFilled(false);
        btnVideo.setOpaque(true);

        if(videoURL.length() == 0){
            btnVideo.setEnabled(false);
        }

        btnVideo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    SearchPanel p = new SearchPanel(dbc);
                try {
                    p.openBrowser(videoURL);
                } catch (URISyntaxException e1) {
                    ErrorHandler er = new ErrorHandler();
                    er.writeLog("PrinterFrame", e1.toString());
                } catch (IOException e1) {
                    ErrorHandler er = new ErrorHandler();
                    er.writeLog("PrinterFrame", e1.toString());
                }
            }
        });

        JCheckBox cbFavorite = new JCheckBox("Set recipe to favorite");
        cbFavorite.setFont(new Font("calibri", Font.BOLD, 14));
        cbFavorite.setBackground(new Color(204,255,255));
        cbFavorite.setContentAreaFilled(false);
        cbFavorite.setOpaque(true);

        cbFavorite.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    int state;
                    if (cbFavorite.isSelected() == true){
                        state = 1;
                    }else{
                        state = 0;
                    }
                    try {
                        //DBConnect db = new DBConnect();
                        //db.setFavorite(id, state);
                        dbc.setFavorite(id, state);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }


            }
        });
        south.add(cbFavorite);
        south.add(btnPrint);
        south.add(btnVideo);
        east.add(label);
        frame.pack();
        frame.getContentPane().add(BorderLayout.CENTER, center);
        frame.getContentPane().add(BorderLayout.SOUTH, south);
        frame.getContentPane().add(BorderLayout.EAST, east);

        try {
            ResultSet rs;
            //DBConnect db = new DBConnect();
            //rs = db.getFavorite(id);
            rs = dbc.getFavorite(id);
            int favorite = rs.getInt(1);
            if (favorite == 1){
                cbFavorite.setSelected(true);
            }else{
                cbFavorite.setSelected(false);
            }
            rs.close();
        } catch (SQLException e) {
            ErrorHandler er = new ErrorHandler();
            er.writeLog("PrinterFrame", e.toString());
        }

        frame.setSize(1300,400);




    }
    

}
