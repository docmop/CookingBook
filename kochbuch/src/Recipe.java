/*
 * @Author cheick sadibou diagne
 *
 */

public class Recipe {
	private int rid;
	private String name;
	private String author;
	private int favorite;
	private int category;
	private String pictures;
	private String video;
	private String ingredients;
	private String instruction;

	public Recipe() {}

	/**
	 *
	 * @param name
	 * @param author
	 * @param favorite
	 * @param category
	 */
	public Recipe(String name, String author, int favorite, int category) {
		this.name = name;
		this.author = author;
		this.favorite = favorite;
		this.category = category;
	}

	/**
	 *
	 * @param rid
	 */
	public Recipe(int rid) {
		this.rid = rid;
	}

	/**
	 * Constructor to create an recipe
	 * @param rid
	 * @param name
	 * @param author
	 * @param instruction
	 * @param favorite
	 * @param category
	 * @param ingredients
	 * @param pictures
	 * @param video
	 */
	public Recipe(int rid, String name, String author, int favorite, int category, String pictures, String video, String ingredients,  String instruction) {
		this.rid = rid;
		this.name = name;
		this.author = author;
		this.instruction = instruction;
		this.favorite = favorite;
		this.category = category;
		this.ingredients = ingredients;
		this.pictures = pictures;
		this.video = video;
	}

	/**
	 * Constructor to create an recipe
	 * @param name
	 * @param author
	 * @param instruction
	 * @param favorite
	 * @param category
	 * @param ingredients
	 * @param pictures
	 * @param video
	 */
	public Recipe(String name, String author, int favorite, int category, String pictures, String video, String ingredients,  String instruction) {
		this.name = name;
		this.author = author;
		this.instruction = instruction;
		this.favorite = favorite;
		this.category = category;
		this.ingredients = ingredients;
		this.pictures = pictures;
		this.video = video;
	}

	// Getter -----------------------------------

	public int getRid() {
		return rid;
	}

	public String getName() {
		return name;
	}

	public String getAuthor() {
		return author;
	}

	public String getInstruction() {
		return instruction;
	}

	public int getFavorite() {
		return favorite;
	}

	public int getCategory() {
		return category;
	}

	public String getIngredients() {
		return ingredients;
	}

	public String getPictures() {
		return pictures;
	}

	public String getVideo() {
		return video;
	}

	// Setter -----------------------------------

	public void setRid(int rid) {
		this.rid = rid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public void setFavorite(int favorite) {
		this.favorite = favorite;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public void setIngredients(String ingredients) {
		this.ingredients = ingredients;
	}

	public void setPictures(String pictures) {
		this.pictures = pictures;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	// Other methods ----------------------------

	public Boolean equals(Recipe contestor) {
		if(this.name != contestor.getName())
			return false;
		if(this.name != contestor.getName())
			return false;
		if(this.author != contestor.getAuthor())
			return false;
		if(this.instruction != contestor.getInstruction())
			return false;
		if(this.favorite != contestor.getFavorite())
			return false;
		if(this.category != contestor.getCategory())
			return false;
		if(this.ingredients != contestor.getIngredients())
			return false;
		if(this.pictures != contestor.getPictures())
			return false;
		if(this.video != contestor.getVideo())
			return false;

		return true;
	}
}