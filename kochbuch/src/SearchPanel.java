
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.validator.routines.UrlValidator;

/**
 * @author joshua, Jeff, Diagne
 */
public class SearchPanel extends JPanel{

	private DBConnect dbc;

	public static String[] buttons = {"Open", "Edit", "Export", "Video", "Shopping list","Delete"};
	public static ResultSet rs;
	public static String[] title = new String[]{
			"RID", "Name", "Author", "Instruction", "Ingredients", "Favorite", "Categorie", "Picture", "Video"
	};

	public static DefaultTableModel model = new DefaultTableModel(title, 0) {
		@Override
		public boolean isCellEditable(int row, int column) {
			//all cells false
			return false;
		}
	};
	public static JTable table = new JTable(model);

	String[] tags = {"Vegetarisch", "Vegan", "Laktosefrei", "Glutenfrei", "Getraenk", "Backwaren", "Normal"};

	JPanel searchPanel = new JPanel();
	JPanel ListPanel = new JPanel();
	JLabel lblName = new JLabel("Name or Instruction");
	JTextField txtName = new JTextField("",20);
	JLabel lblAuthor = new JLabel("Author");
	JTextField txtAuthor = new JTextField();
	JLabel lblFavorite = new JLabel("Favorite");
	JRadioButton rBtnFavorite1 = new JRadioButton("Only Favorites");
	JRadioButton rBtnFavorite2 = new JRadioButton("No Favorites");
	JRadioButton rBtnFavorite3 = new JRadioButton("Both");
	JLabel lblTags = new JLabel("Categorie");
	JComboBox TagList = new JComboBox(tags);
	JButton btnSearch = new JButton("Search");
	JButton btnShowAll = new JButton("Show all");
	JButton btnFavorit = new JButton("Only favorites");


	/**
	 * Shows all Recipes
	 *
	 */
	public void showAll(){
		try {
			Object[] data = new Object[0];
			//DBConnect db = new DBConnect();
			//rs = db.selectAllRezepte();
			rs = dbc.selectAllRezepte();
			model.setRowCount(0);
			while(rs.next()){
				int rid = Integer.parseInt(rs.getString(1));
				String name = rs.getString(2);
				String author = rs.getString(3);
				String instruction = rs.getString(4);
				String ingredients = rs.getString(10);
				int favorite = Integer.parseInt(rs.getString(5));
				int category = Integer.parseInt(rs.getString(6));
				String pictures = rs.getString(7);
				String video = rs.getString(8);
				Recipe allRecipe = new Recipe(rid, name, author, favorite, category, pictures, video, ingredients, instruction);

				data = new Object[]{allRecipe.getRid(), allRecipe.getName(), allRecipe.getAuthor(), allRecipe.getInstruction(), allRecipe.getIngredients()
						, allRecipe.getFavorite(), categorieToString(allRecipe.getCategory()), allRecipe.getPictures(), allRecipe.getVideo()};
				model.addRow(data);
			}
			table.setRowHeight(30);
			rs.close();
		} catch (SQLException ex) {
			ErrorHandler er = new ErrorHandler();
			er.writeLog("Searchpanel", ex.toString());
		}
	}

	/**
	 * Converts category int to String
	 * @param index index of category integer
	 * @return category String
	 */
	public static String categorieToString(int index){
		   if(index == 0){
           	return "Vegetarian";
		   }

		   if(index == 1){
			return "Vegan";
		   }

		   if(index == 2){
			return "Lactose free";
		   }

		   if(index == 3){
			return "Gluten free";
		   }

		   if(index == 4){
			return "Drink";
		   }

		   if(index == 5){
			return "Bakery products";
		   }

		   if(index == 6){
			return "Normal";
		   }
		   return null;
	}

	/**
	 * Converts category int to String
	 * @param category category String
	 * @return index of category integer
	 */
	public static int textToInteger(String category){
		if(category.equals("Vegetarian")){
			return 0;
		}

		if(category.equals("Vegan")){
			return 1;
		}

		if(category.equals("Lactose free")){
			return 2;
		}

		if(category.equals("Gluten free")){
			return 3;
		}

		if(category.equals("Drink")){
			return 4;
		}

		if(category.equals("Bakery products")){
			return 5;
		}

		if(category.equals("Normal")){
			return 6;
		}
		return 7;
	}

	/**
	 * Builds Export-Frame
	 * @param recipeToExport
	 */
	public void buildFrameExport(Recipe recipeToExport){
		ImportExport ie = new ImportExport(dbc);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("XML Document (*.xml)", "xml");
		JFileChooser chooser = new JFileChooser();
		chooser.setFileFilter(filter);
		chooser.addChoosableFileFilter(filter);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setApproveButtonText("Export");

		chooser.setDialogTitle("Export");
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int actionDialog = chooser.showOpenDialog(null);

		if (actionDialog == JFileChooser.APPROVE_OPTION) {
			File xmlFile = new File(chooser.getSelectedFile().getAbsolutePath() + ".xml");
			if (!xmlFile.exists()) {
				ie.exportRecipe(recipeToExport, chooser.getSelectedFile().getAbsolutePath());
				JOptionPane.showMessageDialog(null, "Recipe was successfully exported");
			} else {
				int response = JOptionPane.showConfirmDialog(null,
						"Do you want to replace the existing file?",
						"Replace", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.YES_OPTION) {
					ie.exportRecipe(recipeToExport, chooser.getSelectedFile().getAbsolutePath());
					JOptionPane.showMessageDialog(null,
							"The Recipe was successfully exported.",
							"Information",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					buildFrameExport(recipeToExport);
				}
			}

		}
	}

	/**
	 * method to open links in standard browser
	 * @param url
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	public static void openBrowser(String url) throws URISyntaxException, IOException {

		UrlValidator defaultValidator = new UrlValidator(); // default schemes
		if (url.isEmpty()){
			JOptionPane.showMessageDialog(null,
					"No link was given.",
					"Error",
					JOptionPane.ERROR_MESSAGE);
		} else if (defaultValidator.isValid(url)){
			Desktop.getDesktop().browse(new URI(url));
		} else {
			JOptionPane.showMessageDialog(null,
					"The given link is not valid.",
					"Error",
					JOptionPane.ERROR_MESSAGE);
		}


	}

	/**
	 * Constructor
	 */
	public SearchPanel(DBConnect dbc) {

		this.dbc = dbc;

		TagList.setFont(new Font("calibri", Font.BOLD, 14));
		TagList.setBackground(new Color(204,255,255));
		//TagList.setOpaque(true);

		btnSearch.setFont(new Font("calibri", Font.BOLD, 14));
		btnSearch.setBackground(new Color(204,255,255));
		btnSearch.setContentAreaFilled(false);
		btnSearch.setOpaque(true);


		btnShowAll.setFont(new Font("calibri", Font.BOLD, 14));
		btnShowAll.setBackground(new Color(204,255,255));
		btnShowAll.setContentAreaFilled(false);
		btnShowAll.setOpaque(true);

		btnFavorit.setFont(new Font("calibri", Font.BOLD, 14));
		btnFavorit.setBackground(new Color(204,255,255));
		btnFavorit.setContentAreaFilled(false);
		btnFavorit.setOpaque(true);

		this.setLayout(new GridLayout(1,1));

		searchPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(10,10,10,10);

		searchPanel.setBackground(new Color(0,153, 153));

		ListPanel.setLayout( new BoxLayout(ListPanel, BoxLayout.PAGE_AXIS) );
		ListPanel.setBackground(Color.GREEN);

		gbc.gridx=0;
		gbc.gridy=0;
		searchPanel.add(lblName, gbc);
		lblName.setFont(new Font("calibri", Font.BOLD, 14));
		
		gbc.gridx=1;
		gbc.gridy=0;
		searchPanel.add(txtName, gbc);
		
		gbc.gridx=0;
		gbc.gridy=1;
		searchPanel.add(lblAuthor, gbc);
		lblAuthor.setFont(new Font("calibri", Font.BOLD, 14));
		
		gbc.gridx=1;
		gbc.gridy=1;
		searchPanel.add(txtAuthor, gbc);

		gbc.gridx=0;
		gbc.gridy=2;
		searchPanel.add(lblFavorite, gbc);
		lblFavorite.setFont(new Font("calibri", Font.BOLD, 14));

		rBtnFavorite1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rBtnFavorite1.isSelected()){
					rBtnFavorite2.setSelected(false);
					rBtnFavorite3.setSelected(false);
				}
			}
		});
		gbc.gridx=1;
		gbc.gridy=2;
		searchPanel.add(rBtnFavorite1, gbc);
		rBtnFavorite1.setBackground(new Color(0,153, 153));

		rBtnFavorite2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rBtnFavorite2.isSelected()){
					rBtnFavorite1.setSelected(false);
					rBtnFavorite3.setSelected(false);
				}
			}
		});

		gbc.gridx=2;
		gbc.gridy=2;
		searchPanel.add(rBtnFavorite2, gbc);
		rBtnFavorite2.setBackground(new Color(0,153, 153));

		rBtnFavorite3.setSelected(true);
		rBtnFavorite3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(rBtnFavorite3.isSelected()){
					rBtnFavorite1.setSelected(false);
					rBtnFavorite2.setSelected(false);
				}
			}
		});

		gbc.gridx=3;
		gbc.gridy=2;
		searchPanel.add(rBtnFavorite3, gbc);
		rBtnFavorite3.setBackground(new Color(0,153, 153));

		gbc.gridx=0;
		gbc.gridy=4;
		searchPanel.add(lblTags, gbc);
		lblTags.setFont(new Font("calibri", Font.BOLD, 14));
		
				
		TagList.setSelectedIndex(6);
		gbc.gridx=1;
		gbc.gridy=4;
		searchPanel.add(TagList, gbc);


		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					String name;
					String author;
					String path;
					String videoURL;
					int favorite = 2;
					int category = 0;

					if(txtName.toString().isEmpty())
						name = "";
					else
						name = txtName.getText();

					if(txtAuthor.toString().isEmpty())
						author = "";
					else
						author = txtAuthor.getText();

					if (rBtnFavorite1.isSelected()){
						favorite = 1;
					}

					if (rBtnFavorite2.isSelected()){
						favorite = 0;
					}

					if (rBtnFavorite3.isSelected()){
						favorite = 2;
					}

					if(TagList.getSelectedItem().toString().equals("Vegetarisch"))
						category = 0;
					if(TagList.getSelectedItem().toString().equals("Vegan"))
						category = 1;
					if(TagList.getSelectedItem().toString().equals("Laktosefrei"))
						category = 2;
					if(TagList.getSelectedItem().toString().equals("Glutenfrei"))
						category = 3;
					if(TagList.getSelectedItem().toString().equals("Getraenk"))
						category = 4;
					if(TagList.getSelectedItem().toString().equals("Backwaren"))
						category = 5;
					if(TagList.getSelectedItem().toString().equals("Normal"))
						category = 6;

					if((txtName.getText().length() != 0) || txtAuthor.getText().length() != 0) {
					model.setRowCount(0);
					//DBConnect db = new DBConnect();
					//s = db.selectSpecificRezepte(name, author, favorite, category);
					rs = dbc.selectSpecificRezepte(name, author, favorite, category);
					while(rs.next()){
						int rid = Integer.parseInt(rs.getString(1));
						name = rs.getString(2);
						author = rs.getString(3);
						String instruction = rs.getString(4);
						String ingredients = rs.getString(10);
						favorite = Integer.parseInt(rs.getString(5));
						int categorie = Integer.parseInt(rs.getString(6));
						path = rs.getString(7);
						videoURL = rs.getString(8);
						model.addRow(new Object[]{rid, name, author, instruction, ingredients, favorite, categorieToString(categorie), path, videoURL}); 
					}
					table.setRowHeight(30);
					}else{
						JOptionPane.showMessageDialog(null, "You need to fill the Textfields author or (name or instruction)!");

					}
				} catch (SQLException e1) {
					ErrorHandler er = new ErrorHandler();
					er.writeLog("Searchpanel", e1.toString()); 
				} 
			}
		});

        btnFavorit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    String name;
                    String author;
                    String path;
                    String videoURL;
                    int favorite;
                    int category = 0;

					/*if(txtName.getText().length() != 0) {*/

						model.setRowCount(0);
						//DBConnect db = new DBConnect();
						//rs = db.selectFavorite();
						rs = dbc.selectFavorite();
						while (rs.next()) {
							int rid = Integer.parseInt(rs.getString(1));
							name = rs.getString(2);
							author = rs.getString(3);
							String instruction = rs.getString(4);
							String ingredients = rs.getString(10);
							favorite = Integer.parseInt(rs.getString(5));
							category = Integer.parseInt(rs.getString(6));
							path = rs.getString(7);
							videoURL = rs.getString(8);
							model.addRow(new Object[]{rid, name, author, instruction, ingredients, favorite, categorieToString(category), path, videoURL});
						}
						table.setRowHeight(30);
					/*}else{
						JOptionPane.showMessageDialog(null, "You need to fill a String to search for!");

					}*/

                } catch (SQLException e1) {
					ErrorHandler er = new ErrorHandler();
					er.writeLog("Searchpanel", e1.toString());
                }}
        });

		gbc.gridwidth = 1;
		gbc.gridx=0;
		gbc.gridy=5;
		ImageIcon show = new ImageIcon(System.getProperty("user.dir")+"/Icons/show.png");
		btnShowAll.setIcon(show);
		btnShowAll.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnShowAll.setHorizontalTextPosition(SwingConstants.CENTER);
		searchPanel.add(btnShowAll, gbc);

		gbc.gridwidth = 1;
		gbc.gridx=1;
		gbc.gridy=5;
		ImageIcon search = new ImageIcon(System.getProperty("user.dir")+"/Icons/search.png");
		btnSearch.setIcon(search);
		btnSearch.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnSearch.setHorizontalTextPosition(SwingConstants.CENTER);
		searchPanel.add(btnSearch, gbc);

		gbc.gridwidth = 1;
		gbc.gridx=2;
		gbc.gridy=5;
		ImageIcon fav = new ImageIcon(System.getProperty("user.dir")+"/Icons/favorite.png");
		btnFavorit.setIcon(fav);
		btnFavorit.setVerticalTextPosition(SwingConstants.BOTTOM);
		btnFavorit.setHorizontalTextPosition(SwingConstants.CENTER);
		searchPanel.add(btnFavorit, gbc);

		
		table.getTableHeader().setFont(new Font("calibri", Font.CENTER_BASELINE, 14));
		table.getTableHeader().setForeground(new Color(0,153,153));
		ListPanel.add(new JScrollPane( table ));
		btnShowAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAll();

			}
		});

		table.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				int index = 0;
				ImageIcon icon = new ImageIcon(System.getProperty("user.dir")+"/Icons/icon.png");
				int rid = (int) table.getValueAt(table.getSelectedRow(), 0);
				String name = (String)table.getValueAt(table.getSelectedRow(), 1);
				String author = (String) table.getValueAt(table.getSelectedRow(), 2);
				String instruction = (String)table.getValueAt(table.getSelectedRow(), 3);
				String ingredients =  (String)table.getValueAt(table.getSelectedRow(), 4);
				int favorite = (int) table.getValueAt(table.getSelectedRow(), 5);
				String category = (String) table.getValueAt(table.getSelectedRow(), 6);
				String pictures = (String) table.getValueAt(table.getSelectedRow(), 7);
				String video = (String) table.getValueAt(table.getSelectedRow(), 8);

				int choice = JOptionPane.showOptionDialog(
						    null,
						     name,
						    "Options",
						     JOptionPane.YES_NO_CANCEL_OPTION,
						     JOptionPane.QUESTION_MESSAGE, icon,
						     buttons, "default");
				if(TagList.getSelectedItem().toString().equals("Vegetarisch"))
					index = 0;
				if(TagList.getSelectedItem().toString().equals("Vegan"))
					index = 1;
				if(TagList.getSelectedItem().toString().equals("Laktosefrei"))
					index = 2;
				if(TagList.getSelectedItem().toString().equals("Glutenfrei"))
					index = 3;
				if(TagList.getSelectedItem().toString().equals("Getraenk"))
					index = 4;
				if(TagList.getSelectedItem().toString().equals("Backwaren"))
					index = 5;
				if(TagList.getSelectedItem().toString().equals("Normal"))
					index = 6;

				
				switch (choice) {
					case 0:
						OpenFrame pf = new OpenFrame(dbc);
						pf.buildFrame(rid, name, instruction, ingredients, category, pictures, video, favorite);
						break;
					case 1:
						EditFrame edit = new EditFrame(dbc);
						if(favorite == 1){
							edit.buildEditFrame(rid, name, author, ingredients, instruction, true, index, pictures, video);
						}else {
							edit.buildEditFrame(rid, name, author, ingredients, instruction, false, index, pictures, video);
						}

						break;

					case 2:
						Recipe recipeToExport = new Recipe(name, author, favorite, textToInteger(category), pictures, video, ingredients, instruction);
						buildFrameExport(recipeToExport);


						break;
					case 3:

						try {
							openBrowser(video);
						} catch (URISyntaxException e) {
							ErrorHandler er = new ErrorHandler();
							er.writeLog("Searchpanel", e.toString());
						} catch (IOException e) {
							ErrorHandler er = new ErrorHandler();
							er.writeLog("Searchpanel", e.toString());
						}

						break;
					case 4:
						JEditorPane shoppingList = new JEditorPane();
						shoppingList.setEditable(false);
						shoppingList.setContentType("text/html");
						shoppingList.setText("<h1 align=\"center\"><u><font size=\"10\">Shopping List: </font></u><br><span>"+name+"</span></h1>" +
								             "<pre><font size=\"4\" style=\"font-weight:bold\">"+ingredients+"</font></pre>");

						PreviewDialog p = new PreviewDialog(shoppingList);
						ImageIcon img = new ImageIcon(System.getProperty("user.dir")+"/Icons/cook.png");
						p.setIconImage(img.getImage());
						p.setVisible(true);

						break;

					case 5:
						try {
							//DBConnect db = new DBConnect();
							//db.removeRezepte(rid);
							dbc.removeRezepte(rid);
							showAll();
						} catch (SQLException e) {
							ErrorHandler er = new ErrorHandler();
							er.writeLog("Searchpanel", e.toString());
						}
						break;

				}

			}
		});

		this.add(searchPanel);
		this.add(ListPanel);
	}

}
